package com.training.AcceptorDonarHospital;
/**
* Use Case Name:DFTE_FT_M14135_BLOODBANKSYSTEM
* Developer Name:Supritha k
* Squad Number:9
*/

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class AcceptorDonarHospitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcceptorDonarHospitalApplication.class, args);
	}

}
