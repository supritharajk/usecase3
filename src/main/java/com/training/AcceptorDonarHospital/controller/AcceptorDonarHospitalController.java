package com.training.AcceptorDonarHospital.controller;
/**
* Use Case Name:DFTE_FT_M14135_BLOODBANKSYSTEM
* Developer Name:Supritha k
* Squad Number:9
*/

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.training.AcceptorDonarHospital.model.Acceptor;
import com.training.AcceptorDonarHospital.model.Donar;
import com.training.AcceptorDonarHospital.service.AcceptorDonarHospitalService;

@RestController
@RequestMapping("/acceptordonarshospital")// http://localhost:5796/acceptordonarshospital
public class AcceptorDonarHospitalController {
	
	// autowire the AcceptorDonarService class
	@Autowired
	AcceptorDonarHospitalService acceptordonarhospitalService;
	
	
		// creating a get mapping that retrieves all the Acceptor detail from the
		// database
	//http://localhost:5796/acceptordonarshospital/all
	@GetMapping("/all")
	public List<Acceptor> getDetails(){
		return acceptordonarhospitalService.getDetails();
	}
	
	// creating post mapping that post the acceptor detail in the database
		// http://localhost:5796/acceptordonarshospital/add
	@PostMapping("/add")
	public Acceptor createAcceptorsDonars(@RequestBody Acceptor acceptor) {
		return acceptordonarhospitalService.createAcceptorsDonars(acceptor);
		
	}
	
	// creating a get mapping that retrieves the detail of a specific acceptor
	//http://localhost:5796/acceptordonarshospital/getallbyid
	@GetMapping("/getallbyid")
	public Acceptor getAllById(@RequestParam int id) {
		return acceptordonarhospitalService.getAllById(id);
		
	}
	
	// creating a delete mapping that deletes a specified acceptor
		//  http://localhost:5796/acceptordonarshospital/deleteallbyid-using soapui and choose method=DELETE
	@DeleteMapping("/deleteallbyid")
	public String deleteAllById(@RequestParam int id) {
		 return acceptordonarhospitalService.deleteAllById(id);
		 
	}
	
	// creating put mapping that updates the acceptor detail
		//  http://localhost:5796/acceptordonarshospital/updateAll -using soapui and choose method=UPDATE
	@PutMapping("/updateAll")
	public Acceptor updateAcceptorDonars(@RequestBody Acceptor acceptor) {
		return acceptordonarhospitalService.updateAcceptorDonars(acceptor);
		
	}
	
	
	
	

}
