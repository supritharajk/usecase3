package com.training.AcceptorDonarHospital.service;
/**
* Use Case Name:DFTE_FT_M14135_BLOODBANKSYSTEM
* Developer Name:Supritha k
* Squad Number:9
*/

import java.util.List;

import com.training.AcceptorDonarHospital.model.Acceptor;
import com.training.AcceptorDonarHospital.model.Donar;

public interface AcceptorDonarHospitalService {

	public List<Acceptor> getDetails();

	public Acceptor createAcceptorsDonars(Acceptor acceptor);

	public Acceptor updateAcceptorDonars(Acceptor acceptor);

	public Acceptor getAllById(int id);

	public String deleteAllById(int id);

}
