//**Creating Service Interface
// Write the UnImplemented methods in main class(ServiceImp)

package com.training.AcceptorDonarHospital.service;
/**
* Use Case Name:DFTE_FT_M14135_BLOODBANKSYSTEM
* Developer Name:Revathy L
* Squad Number:9
*/

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.training.AcceptorDonarHospital.model.Hospital;

@FeignClient(value="hospitalApp",url="http://localhost:9999/hospital")
public interface HospitalServices {

	@PostMapping("/create")
	public Hospital createHospital(@RequestBody Hospital hospital);
	
	@PutMapping("/update")
	public Hospital updateHospital(@RequestBody Hospital hospital);
	
	@DeleteMapping("")
	public String deleteHospital(@RequestParam("id") int id);
	
	@GetMapping("/getById")
	public Hospital getHospital(@RequestParam("id") int id);
	
	@GetMapping("/all")
	public List<Hospital> getHospitals();
	
	@GetMapping("/donarId")
	public List<Hospital> getByAcceptorId(@RequestParam("id") int id);
	
	@PostMapping("")
	public Hospital createHospitals(@RequestBody Hospital hospital);
	
	@DeleteMapping("/delete")
	public String deleteByAcceptorId(@RequestParam("id") int id);
}

