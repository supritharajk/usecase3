/**
* creating Service Interface
* to write the UnImplemented methods in main class(ServiceImp)
*/
package com.training.AcceptorDonarHospital.service;
/**
* Use Case Name:DFTE_FT_M14135_BLOODBANKSYSTEM
* Developer Name:Supritha k
* Squad Number:9
*/

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.training.AcceptorDonarHospital.model.Acceptor;

@FeignClient(value="acceptorApp",url = "http://localhost:7777/acceptor")
public interface AcceptorService {

	@PostMapping("/add")
	public Acceptor createAcceptor(@RequestBody Acceptor acceptor);

	@PutMapping("/update")
	public Acceptor updateAcceptor(@RequestBody Acceptor acceptor);

	@DeleteMapping("/delete")
	public String deleteAcceptor(@RequestParam("id") int id);

	@GetMapping("/getbyid")
	public Acceptor getAcceptor(@RequestParam("id") int id);

	@GetMapping("/allAcceptor")
	public List<Acceptor> getAcceptors();

}
