package com.training.AcceptorDonarHospital.service;
/**
* Use Case Name:DFTE_FT_M14135_BLOODBANKSYSTEM
* Developer Name:Supritha k
* Squad Number:9
*/

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.AcceptorDonarHospital.model.Acceptor;
import com.training.AcceptorDonarHospital.model.Donar;
import com.training.AcceptorDonarHospital.model.Hospital;

@Service
public class AcceptorDonarHospitalServiceImpl implements AcceptorDonarHospitalService {

	@Autowired
	private AcceptorService acceptorService;

	@Autowired
	private DonarService donarService;

	@Autowired
	private HospitalServices hospitalService;

	// getting all acceptordonarshospital record by using the method findaAll() of CrudRepository
	@Override
	public List<Acceptor> getDetails() {
		List<Acceptor> acceptors = acceptorService.getAcceptors();
		acceptors.forEach(a -> {
			a.setHospitalList(hospitalService.getByAcceptorId(a.getId()));
			a.setDonarList(donarService.getDonars(a.getId()));
		});
		return acceptors;
	}

	// saving a specific record by using the method save() of JpaRepository
	@Override
	public Acceptor createAcceptorsDonars(Acceptor acceptor) {
		Acceptor acceptor2 = acceptorService.createAcceptor(acceptor);
		acceptor.getDonarList().forEach(d -> {
			d.setAcceptorId(acceptor2.getId());
			donarService.createBloodManagement(d);
		});
		acceptor.getHospitalList().forEach(h -> {
			h.setAcceptorId(acceptor2.getId());
			hospitalService.createHospitals(h);
		});
		acceptor2.setDonarList(donarService.getDonars(acceptor2.getId()));
		acceptor2.setHospitalList(hospitalService.getByAcceptorId(acceptor2.getId()));
		return acceptor2;
	}

	// getting a specific record by using the method findById() of JpaRepository
	@Override
	public Acceptor getAllById(int id) {
		Acceptor acceptor = acceptorService.getAcceptor(id);
		List<Donar> donar = donarService.getDonars(acceptor.getId());
		List<Hospital> hospitals = hospitalService.getByAcceptorId(acceptor.getId());
		acceptor.setDonarList(donar);
		acceptor.setHospitalList(hospitals);
		return acceptor;
	}

	// deleting a specific record by using the method deleteById() of JpaRepository
	@Override
	public String deleteAllById(int id) {
		Acceptor acceptor = acceptorService.getAcceptor(id);
		if (acceptor != null) {
			String b = donarService.deleteByAcceptorId(acceptor.getId());
			String c = hospitalService.deleteByAcceptorId(acceptor.getId());
			String a = acceptorService.deleteAcceptor(id);

			return a;
		}
		return "id is not present";
	}

	// updating a record
	@Override
	public Acceptor updateAcceptorDonars(Acceptor acceptor) {

		Acceptor acceptor2 = acceptorService.updateAcceptor(acceptor);
		acceptor.getDonarList().forEach(d -> {
			donarService.updateBloodManagement(d);
		});
		acceptor.getHospitalList().forEach(h -> {
			hospitalService.updateHospital(h);
		});
		acceptor2.setDonarList(donarService.getDonars(acceptor2.getId()));
		acceptor2.setHospitalList(hospitalService.getByAcceptorId(acceptor2.getId()));
		return acceptor2;
	}

}
