/**
 * 
 */
package com.training.AcceptorDonarHospital.model;

/**
 * @author revathy_l
 *
 */
import java.time.LocalDate;


import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Past;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor


//Required fields for hospital table

public class Hospital {
	
	private Integer id;
   @Length(min = 4)
	@NotNull(message = "hospital_name must not be null")
	private String hospitalName;
	@Length(min = 5) 
	private String address;
	@Length(min = 5)
	@NotNull
	private String email;
	private int acceptorId;
	}
	
	

