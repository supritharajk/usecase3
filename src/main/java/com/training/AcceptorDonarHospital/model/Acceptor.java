/**
* Creating model package for BloodBankManagement System
*
*/
package com.training.AcceptorDonarHospital.model;
/**
* Use Case Name:DFTE_FT_M14135_BLOODBANKSYSTEM
* Developer Name:Supritha k
* Squad Number:9
*/

import java.util.List;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
// mark class as an Entity
// defining Table name
public class Acceptor {

	// Required fields for Acceptor table
	// {acceptor_id,name,age,bloodGroup,gender,address,email,password}

	// Defining acceptor id as primary key
	private Integer id;
	@Length(min = 5)
	private String name;
	@Min(value = 18, message = "age should greaterthan or equal to 18 ")
	private int age;
	private String bloodGroup;
	private char gender;
	private String address;
	private String email;
	private List<Donar> donarList;
	private List<Hospital> hospitalList;

	

}
