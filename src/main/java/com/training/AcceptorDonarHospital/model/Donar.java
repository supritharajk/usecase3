/**
 * Creating model package for BloodManagement  System 
 * 
 */
package com.training.AcceptorDonarHospital.model;
/**
 * @author akhilesh-a
 *Use Case Name:Blood Bank Management system
 *Squad Number:9
 */

import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Donar {
	
	//Required fields for donar table
	//{donor_id,name,email,address,blood_group,age,contact}
//	private Integer donor_id;
	private Integer id;
	@Length(min = 3)
	private String name;
	
	@Email(message = "Email is not valid", regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
	@NotEmpty(message = "Email cannot be empty")
	private String email;
	private String address;
	@NotNull
	private String bloodGroup;
	@Min(value = 15)
	private float age;
	private long contact;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
	@Past
	private LocalDate dateOfBirth;
	private int acceptorId;
//	private List<Hospital> hospitalList;

	
	
	
	
	
}
